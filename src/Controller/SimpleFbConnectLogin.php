<?php
/**
 * @file
 * Contains \Drupal\simple_fb_connect\Controller\SimpleFbConnectLogin.
 */
 
namespace Drupal\simple_fb_connect\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * DemoController.
 */
class SimpleFbConnectLogin extends ControllerBase {

  /**
   * Page callback for the FB Connect URL.
   */
  public function simple_fb_connect_login() {
    $account = $this->currentUser();
    if ($account->id() != 0) {
      drupal_access_denied();
    }
    $facebook = facebook_client();
    $acc = $facebook->getAccessToken();
    $fb_user = $facebook->getUser();
    
    if ($fb_user) {
      $fb_user_profile = $facebook->api('/me');
      if (isset($fb_user_profile['email'])) {
        $query = db_select('users', 'u');
        $query->condition('u.mail', check_plain($fb_user_profile['email']));
        $query->fields('u', array('uid'));
        $query->range(0, 1);

        $drupal_user_id = 0;
        $result = $query->execute();
        foreach ($result as $record) {
          $drupal_user_id = $record->uid;
        }

        if ($drupal_user_id) {
          $user_obj = user_load($drupal_user_id);
          if ($user_obj->status) {
            $form_state = array();
            $form_state['uid'] = $drupal_user_id;
            
            user_login_finalize($user_obj);
            
            //user_login_submit(array(), $form_state);
            drupal_set_message(t('You have been logged in.'));
            
            $redirect_url_post_login = \Drupal::config('simple_fb_connect.settings')->get('simple_fb_connect_post_login_url');
            
            header('Location: '. $redirect_url_post_login);
            die();
            // @TODO: Use proper redirect here.. The above header() code needs to be fixed & die() removed.
            //$this->redirect($login_url);
            //$this->redirect($redirect_url_post_login);
          }
          else {
            drupal_set_message(t('You could not be logged in as your account is blocked. Contact site administrator.'), 'error');
            return $this->redirect('user');
          }
        }
        else {
          if (!\Drupal::config('simple_fb_connect.settings')->get('simple_fb_connect_login_only')) {
            //create the drupal user
            //This will generate a random password, you could set your own here
            $fb_username = (isset($fb_user_profile['username']) ? $fb_user_profile['username'] : $fb_user_profile['name']);
            $drupal_username_generated = simple_fb_connect_unique_user_name(check_plain($fb_username));

            $password = user_password(8);
            //set up the user fields
            $fields = array(
              'name' => $drupal_username_generated,
              'mail' => check_plain($fb_user_profile['email']),
              'pass' => $password,
              'status' => 1,
              'init' => 'email address',
              'roles' => array(
                DRUPAL_AUTHENTICATED_RID => 'authenticated user',
              ),
            );
            // @TODO: This section is not in use - Add it.
            if (\Drupal::config('simple_fb_connect.settings')->get('user_pictures')) {
              $dimensions_in_text = \Drupal::config('simple_fb_connect.settings')->get('user_picture_dimensions');
              $dimensions = explode('x', $dimensions_in_text);
              if (count($dimensions) == 2) {
                $width = $dimensions[0];
                $height = $dimensions[1];
              }
              else {
                $width = SIMPLE_FB_CONNECT_DEFAULT_WIDTH;
                $height = SIMPLE_FB_CONNECT_DEFAULT_HEIGHT;
              }
              $pic_url = "https://graph.facebook.com/" . check_plain($fb_user_profile['id']) . "/picture?width=$width&height=$height";
              $response = drupal_http_request($pic_url);
              $file = 0;
              if ($response->code == 200) {
                $picture_directory = file_default_scheme() . '://' . \Drupal::config('system.site')->get('user_picture_path');
                file_prepare_directory($picture_directory, FILE_CREATE_DIRECTORY);
                $file = file_save_data($response->data, $picture_directory . '/' . check_plain($fb_user_profile['id'] . '.jpg', FILE_EXISTS_RENAME));
              }
              if (is_object($file)) {
                $fields['picture'] = $file->fid;
              }
            }


            //the first parameter is left blank so a new user is created
            
            //$account = user_save('', $fields);
            $account = entity_create('user', $fields);
            //print_r($account);exit;
            $account->save();
            // If you want to send the welcome email, use the following code
            // Manually set the password so it appears in the e-mail.
            //$account->password = $fields['pass'];
            
            // Send the e-mail through the user module.
            drupal_mail('user', 'register_no_approval_required', $fields['mail'], NULL, array('account' => $account), \Drupal::config('system.site')->get('site_mail'));
            //drupal_set_message(t('You have been registered to the site.'));
            
            // Redirect to the same page to login after register.
            header('Location: http://' . $_SERVER['HTTP_HOST'] . request_uri());
            die();
          }
          else {
            drupal_set_message(t('There was no account with the email addresse !email found. Please register before trying to login.', array('!email' => check_plain($fb_user_profile['email']))), 'error');
            return $this->redirect('<front>');
          }
        }
      }
      else {
        drupal_set_message(t('Though you have authorised the Facebook app to access your profile, you have revoked the permission to access email address. Please contact site administrator.'), 'error');
        return $this->redirect('<front>');
      }
    }
    else {
      if (!isset($_REQUEST['error'])) {
        $config = \Drupal::config('simple_fb_connect.settings');
        if ($config->get('simple_fb_connect_appid')) {
          $login_url_params = array(
            'scope' => 'email',
            'fbconnect' => 1,
            'redirect_uri' => 'http://' . $_SERVER['HTTP_HOST'] . request_uri(),
          );
          $login_url = $facebook->getLoginUrl($login_url_params);
          header('Location: '. $login_url);
          die();
          // @TODO: Use proper redirect here.. The above header() code needs to be fixed & die() removed.
          //$this->redirect($login_url);
        }
        else {
          drupal_set_message(t('Facebook App ID Missing. Can not perform Login now. Contact Site administrator.'), 'error');
          return $this->redirect('<front>');
        }
      }
      else {
        if ($_REQUEST['error'] == SIMPLE_FB_CONNECT_PERMISSION_DENIED_PARAMETER) {
          drupal_set_message(t('Could not login with facebook. You did not grant permission for this app on facebook to access your email address.'), 'error');
        }
        else {
          drupal_set_message(t('There was a problem in logging in with facebook. Contact site administrator.'), 'error');
        }
        return $this->redirect('<front>');
      }
    }
  }   
  
  
  
}
